using System;
using Microsoft.Xna.Framework;
using ModopeGame.API.Events;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Graphics.Text;
using ModopeGame.API.Modules;
using TestModule;

namespace Lidor
{
	public class MyClass : Module
	{
		public MyClass()
		{
		}

		public void Render(RenderEvent e)
		{
			e.Canvas.renderHello();
		}

		public void Init(InitializationEvent e)
		{
			
		}

		public void Update(UpdateEvent e)
		{
			
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace test
{
	public class Camera : GameComponent
	{
		private GameTime gameTime;

		public float _yaw = 0, _pitch = 0, _roll = 0; //Rotate

		private Vector3 _camPos; //Starting position

		//Mouse
		//private MouseState _curMouseState;
		//private MouseState _prevMouseState;

		//Properties
		Vector3 Dir { 
			get
			{
				float rad_yaw = -MathHelper.ToRadians(this._yaw);
				float rad_pitch = -MathHelper.ToRadians(this._pitch);
				float rad_roll = -MathHelper.ToRadians(this._roll);
				return Vector3.Transform(Vector3.UnitZ, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
			}
		}

		Vector3 Up {
			get
			{
				float rad_yaw = -MathHelper.ToRadians(this._yaw);
				float rad_pitch = -MathHelper.ToRadians(this._pitch);
				float rad_roll = -MathHelper.ToRadians(this._roll);
				return Vector3.Transform(Vector3.UnitY, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
			}
		}

		Vector3 target { get { return this._camPos + this.Dir; } } //Caculates the Vector 3 of the camera and where its looking at

		//Matrices
		Matrix view { get { return Matrix.CreateLookAt(this._camPos, target, this.Up); } }
		Matrix projection;

		public Camera(Game game, GameTime gameTime, int FOV, float renderDistanceMin, float renderDistanceMax, 
		              Vector3 pos = default(Vector3)) : base(game)
		{
			this._camPos = pos;
			this.gameTime = gameTime;

			this.projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(FOV), 16f / 9f, renderDistanceMin, renderDistanceMax);
		}

		//public void mousemov()//put this one in update
		//{
		//	if (this._curmousestate != this._prevmousestate)
		//	{
		//		float dx = this._prevmousestate.x - this._curmousestate.x;
		//		float dy = this._prevmousestate.y - this._curmousestate.y;
		//		const int inversey = 1;
		//		const int inversex = -1;
		//		this._pitch += dy * (float)gametime.elapsedgametime.totalseconds * this._sensitivity * inversey;
		//		this._yaw += dx * (float)gametime.elapsedgametime.totalseconds * this._sensitivity * inversex;
		//		//originalmousestate = mouse;
		//		mouse.setposition(graphicsdevice.viewport.width / 2, graphicsdevice.viewport.height / 2);
		//	}
		//}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

//Thanks to Matt Guerrette for his Youtube on creating a camera in XNA

namespace Camera_and_Skybox
{
    class Camera : GameComponent
    {
        private Vector3 _position;
        private Vector3 _rotation;
        private Vector3 _lookAt;
        private float _speed;

		private Vector3 _mouseRotationBuffer;
		private MouseState _curMouseState;
		private MouseState _prevMouseState;

       //Properties
       public Vector3 Position {
            get { return this._position; }
            set 
            {
                this._position = value;
                UpdateLookAt(); //Must update after any changes to the camera
            }
        }

        public Vector3 Rotation {
            get { return this._rotation; }
            set 
            {
                this._rotation = value;
                UpdateLookAt(); //Must update after any changes to the camera
            }
        }

       public Matrix Projection {
            get;
            set;
        }

        public Matrix View {
            get 
            {
                return Matrix.CreateLookAt(this._position, this._lookAt, Vector3.Up); //Returns where's the camera located
            }
        }


        //Constractor
        public Camera(Game game, Vector3 position, Vector3 rotation, float speed) : base(game) 
        {
            this._speed = speed;

			//Set the projection Matrix
			float closeFOV = 0.07f;
			float farFOV = 500.0f;
			Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, game.GraphicsDevice.Viewport.AspectRatio, closeFOV, farFOV); //PO4 is like 45 degries | The floats are like view distance, one for close and one for far

            //Set position and rotation
            MoveTo(position, rotation);

			this._prevMouseState = Mouse.GetState();
        }

        //Change the position and rotetion of the camera, and update the look
        private void MoveTo(Vector3 pos, Vector3 rot)
        {
            Position = pos; 
            Rotation = rot;
        }

        //Update look on Vector
        private void UpdateLookAt()
        {
            //Build rotation matrix
            Matrix rotationMatrix = Matrix.CreateRotationX(this._rotation.X) * Matrix.CreateRotationY(this._rotation.Y);
            //Build loot-at vector
            Vector3 lookAtOffset = Vector3.Transform(Vector3.UnitZ, rotationMatrix);
            //Update camera look-at vector
            this._lookAt = this._position + lookAtOffset;
        }

		//Movement
		private Vector3 PreMove(Vector3 amount)
		{
			//Rotate matrix
			Matrix rotate = Matrix.CreateRotationY(this._rotation.Y);
			//movment Vector
			Vector3 movment = amount;
			movment = Vector3.Transform(movment, rotate);
			//Return the Camera position
			return this._position + movment;
		}

		private void Move(Vector3 scale)
		{
			MoveTo(PreMove(scale), Rotation);
		}

        //Update 
        public override void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

			this._curMouseState = Mouse.GetState();

			KeyboardState ks = Keyboard.GetState();

			//Key movment
			Vector3 moveVec = Vector3.Zero;
			if (ks.IsKeyDown(Keys.W) && !ks.IsKeyDown(Keys.S))
				moveVec.Z = 2;
			if (ks.IsKeyDown(Keys.S) && !ks.IsKeyDown(Keys.W))
				moveVec.Z = -2;
			if (ks.IsKeyDown(Keys.A) && !ks.IsKeyDown(Keys.D))
				moveVec.X = 1;
			if (ks.IsKeyDown(Keys.D) && !ks.IsKeyDown(Keys.A))
				moveVec.X = -1;
			if(ks.IsKeyDown(Keys.Space) && !ks.IsKeyDown(Keys.LeftControl))
				moveVec.Y = 1;
			if (ks.IsKeyDown(Keys.LeftControl) && !ks.IsKeyDown(Keys.Space))
				moveVec.Y = -1;

			   
			if(moveVec !=Vector3.Zero)
			{
				//makes sure that you dont move faster diagnoally
				moveVec.Normalize();
				moveVec *= dt * this._speed;

				//Send the new Vector3 values to the move functions
				Move(moveVec);

				//Mouse movement
				float changeX;
				float changeY;

				if(this._curMouseState != this._prevMouseState)
				{
					changeX = this._curMouseState.X - (Game.GraphicsDevice.Viewport.Width / 2);
					changeY = this._curMouseState.Y - (Game.GraphicsDevice.Viewport.Height / 2);
					this._mouseRotationBuffer.X -= 0.01f * changeX * dt;
                    this._mouseRotationBuffer.Y -= 0.01f * changeY * dt;

					if (this._mouseRotationBuffer.Y < MathHelper.ToRadians(-75.0f))
						this._mouseRotationBuffer.Y = this._mouseRotationBuffer.Y - (this._mouseRotationBuffer.Y - MathHelper.ToRadians(-75.0f));
				}

			}

			this._prevMouseState = this._curMouseState;

            base.Update(gameTime);


        }
    }
}

﻿using System;
using Camera_and_Skybox;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XnaModel = Microsoft.Xna.Framework.Graphics.Model;

namespace test
{
	public class Model : GameComponent
	{
		public XnaModel XnaModel {
			get;
			set;
		}


		//Vector 3s
		public Vector3 Position { // X position, Y position, Z position
			get;
			set;
		}
		public Vector3 Rotation { // X rotation, Y rotation, Z rotation
			get;
			set;
		}
		public Vector3 Scale { // X scale, Y scale, Z scale
			get;
			set;
		}

		public Model Parent {
			get;
			set;
		}

		/*private Matrix worldMatrix = default(Matrix); // null
		public Matrix WorldMatrix {
			get
			{
				if (worldMatrix == default(Matrix)) //if null
						worldMatrix = default(Matrix);
				 	return worldMatrix;
		}

		[EventHandler]
		public void OnUpdate(UpdateEvent e)
		{
				worldMatrix = default(Matrix); //null
		}*/



		public Matrix WorldMatrix {
			get
			{
				float rad_yaw = MathHelper.ToRadians(this.Rotation.Y);
				float rad_pitch = MathHelper.ToRadians(this.Rotation.X);
				float rad_roll = MathHelper.ToRadians(this.Rotation.Z);

				Matrix world = Matrix.CreateScale(this.Scale)
									 * Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll)
									 * Matrix.CreateTranslation(this.Position);
				if (this.Parent != null)
					world *= this.Parent.WorldMatrix;

				return world;
			}
		}

		public Vector3 Direction {
			get
			{
				float rad_yaw = MathHelper.ToRadians(this.Rotation.Y);
				float rad_pitch = MathHelper.ToRadians(this.Rotation.X);
				float rad_roll = MathHelper.ToRadians(this.Rotation.Z);

				return Vector3.Transform(Vector3.UnitZ, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
			}
			set
			{
				float pitch = (float)Math.Acos(value.Y);
				float yaw = (float)Math.Atan2(value.X, -value.Z);
				yaw = yaw < 0 ? 2 * (float)Math.PI + yaw : yaw;
				Rotation = new Vector3(pitch, yaw, 0);
			}
		}

		//Constractor
		public Model(Game game, XnaModel model, Model parent = null,
					 Vector3 position = default(Vector3), Vector3 rotation = default(Vector3), Vector3 scale = default(Vector3)) : base(game)
		{
			this.XnaModel = model;

			this.Parent = parent;
			this.Position = position * (Parent == null ? Vector3.One : Vector3.One / this.Parent.Scale);
			this.Rotation = rotation;
			this.Scale = scale;
		}

		public Model(Game game, string path, ContentManager content, Model parent = null,
			 Vector3 position = default(Vector3), Vector3 rotation = default(Vector3), Vector3 scale = default(Vector3))
			: this(game, content.Load<XnaModel>(path), parent, position, rotation, scale) { }


		//helpers
		private Vector3 GetStrafe(float yawOffset)
		{
			float rad_yaw = -MathHelper.ToRadians(yawOffset);
			Vector3 transformed = Vector3.Transform(this.Direction, Matrix.CreateFromYawPitchRoll(rad_yaw, 0, 0));
			return new Vector3(transformed.X, 0, transformed.Z);
		}

		//Moves
		public Model Translate(Vector3 translation, bool relative = false) //Move model using Vector3
		{
			if (relative)
			{
				float rad_yaw = MathHelper.ToRadians(this.Rotation.Y);
				float rad_pitch = MathHelper.ToRadians(this.Rotation.X);
				float rad_roll = MathHelper.ToRadians(this.Rotation.Z);

				translation = Vector3.Transform(translation, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
			}
			this.Position += translation * (Parent == null ? Vector3.One : Vector3.One / this.Parent.Scale);
			return this;
		}

		public Model Translate(float x, float y, float z, bool relative = false) //Move model using Vector3
		{
			return Translate(new Vector3(x, y, z), relative);
		}

		public Model Move(float amount, Direction direction = test.Direction.Forwards)
		{
			Vector3 movement = Vector3.Zero;
			if (direction == (test.Direction.Forwards))
				movement += Direction * amount;
			if (direction == (test.Direction.Backwards))
				movement -= Direction * amount;
			if (direction == (test.Direction.Left))
				movement += GetStrafe(-90) * amount;
			if (direction == (test.Direction.Right))
				movement += GetStrafe(90) * amount;
			return Translate(movement);
		}

		public void Dismount()
		{
			if (this.Parent == null)
				return;

			Vector3 scale;
			Quaternion quaternion;
			Vector3 translation;
			this.WorldMatrix.Decompose(out scale, out quaternion, out translation);

			this.Scale = scale;
			this.Rotation = this.Rotation + this.Parent.Rotation;
			this.Position = translation;
			this.Parent = null;
		}
	}

	public enum Direction
	{
		Forwards,
		Backwards,
		Up,
		Down,
		Left,
		Right
	}
}

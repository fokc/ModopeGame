﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Camera_and_Skybox
{
    class Floor
    {
        private int _width;
        private int _height;
        private VertexBuffer _buffer;
        private GraphicsDevice _device;
		private Color[] _color = new Color[2] { Color.Green, Color.Lime };

        //Constructor
        public Floor(GraphicsDevice device, int width, int height)
        {
            this._device = device;
            this._width = width;
            this._height = height;
			BuildFloorBuffer();
        }

        //Build Vertex buffer
        private void BuildFloorBuffer()
		{
			List<VertexPositionColor> vertexList = new List<VertexPositionColor>();
			int counter = 0;

			//Loop through to create floor
			for (int x = 0; x < this._width; x++)
			{
				counter++;
				for (int z = 0; z < this._height; z++)
				{
					counter++;

					//Loop through and add vertices
					foreach(VertexPositionColor vertex in FloorTile(x, z, this._color[counter % 2])) // will switch the color every loop
					{
						vertexList.Add(vertex);

					}
				}
			}

			//Create floor buffer
			this._buffer = new VertexBuffer(this._device, VertexPositionColor.VertexDeclaration, vertexList.Count, BufferUsage.None);
			this._buffer.SetData<VertexPositionColor>(vertexList.ToArray());
		}

		//Defines a single tile in our floor
		private List<VertexPositionColor> FloorTile(int xOffset, int zOffset, Color tileColor)
		{
			List<VertexPositionColor> vList = new List<VertexPositionColor>();
			vList.Add(new VertexPositionColor (new Vector3(0 + xOffset, 0, 0 + zOffset), tileColor));
			vList.Add(new VertexPositionColor (new Vector3(1 + xOffset, 0, 0 + zOffset), tileColor));
			vList.Add(new VertexPositionColor (new Vector3(0 + xOffset, 0, 1 + zOffset), tileColor));
			vList.Add(new VertexPositionColor (new Vector3(1 + xOffset, 0, 0 + zOffset), tileColor));
			vList.Add(new VertexPositionColor (new Vector3(1 + xOffset, 0, 1 + zOffset), tileColor));
			vList.Add(new VertexPositionColor (new Vector3(0 + xOffset, 0, 1 + zOffset), tileColor));
			return vList;
		}

		//Draw
		public void Draw(Matrix View, Matrix Projection, BasicEffect effect)
		{
			effect.VertexColorEnabled = true;
			effect.View = View;
			effect.Projection = Projection;
			effect.World = Matrix.Identity;

			//Loop and draw tiles
			foreach (EffectPass pass in effect.CurrentTechnique.Passes)
			{
				pass.Apply();
				this._device.SetVertexBuffer(this._buffer);
				this._device.DrawPrimitives(PrimitiveType.TriangleList, 0, this._buffer.VertexCount / 3); //devide number of vertexes by 3 to know how many triangles we have
			}
		}
    }
}

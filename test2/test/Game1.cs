﻿using System;
using Camera_and_Skybox;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace dtest
{
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		BasicEffect effect;
		SpriteFont debugFont;


		Floor floor;
		Vector3 pos = new Vector3(15, 1, 0); //Camera's world(position) - This line is the starting point

		Vector3 mDir {
			get
			{
				float rad_yaw = MathHelper.ToRadians(modelRotation.Y);
				float rad_pitch = MathHelper.ToRadians(modelRotation.X);
				float rad_roll = MathHelper.ToRadians(modelRotation.Z);
				return Vector3.Transform(Vector3.UnitZ, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
			}
		}

		float yaw = 0, pitch = 0, roll = 0;
		Vector3 dir {
			get
			{
				float rad_yaw = -MathHelper.ToRadians(yaw);
				float rad_pitch = -MathHelper.ToRadians(pitch);
				float rad_roll = -MathHelper.ToRadians(roll);
				return Vector3.Transform(Vector3.UnitZ, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
			}
		}
		Vector3 up {
			get
			{
				float rad_yaw = -MathHelper.ToRadians(yaw);
				float rad_pitch = -MathHelper.ToRadians(pitch);
				float rad_roll = -MathHelper.ToRadians(roll);
				return Vector3.Transform(Vector3.UnitY, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
			}
		}
		Vector3 target { get { return pos + dir; } }

		Vector3 modelPos = new Vector3(15, 0f, 15); // X position, Y position, Z position
		Vector3 modelRotation = new Vector3(0, 0, 0); // X rotation, Y rotation, Z rotation
		Vector3 modelScale = new Vector3(1, 1, 1) * 0.001f; // X scale, Y scale, Z scale
		Matrix modelWorld {
			get
			{
				return Matrix.CreateScale(knuckles.Scale)
							 * Matrix.CreateRotationX(MathHelper.ToRadians(knuckles.Rotation.X))
							 * Matrix.CreateRotationY(MathHelper.ToRadians(knuckles.Rotation.Y))
							 * Matrix.CreateRotationZ(MathHelper.ToRadians(knuckles.Rotation.Z))
							 * Matrix.CreateTranslation(knuckles.Position);
			}
		}

		//Matrix world = Matrix.CreateRotationX(MathHelper.ToRadians(-90)) * Matrix.CreateTranslation(0, 0, 10);
		Matrix view { get { return Matrix.CreateLookAt(pos, target, up); } }
		Matrix projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(60), 16f / 9f, 0.1f, 500f);

		test.Model knuckles;
		test.Model knife;

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

			graphics.PreferredBackBufferWidth = 2560;
			graphics.PreferredBackBufferHeight = 1440;
			graphics.IsFullScreen = true;
			//vsync
			//graphics.SynchronizeWithVerticalRetrace = false;
			//this.IsFixedTimeStep = false;

		}

		protected override void Initialize()
		{
			IsFixedTimeStep = false;
			base.Initialize();
		}

		private void initKnife()
		{
			Vector3 knifePosition = new Vector3(0.25f, 0.22f, 0.05f);
			Vector3 knifeRotation = new Vector3(180f, 0f, 0f);
			knife = new test.Model(this, "knife", Content, knuckles, knifePosition, knifeRotation, Vector3.One * 0.35f);
		}

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			floor = new Floor(GraphicsDevice, 30, 30);
			effect = new BasicEffect(GraphicsDevice);
			debugFont = Content.Load<SpriteFont>("debugFont");

			knuckles = new test.Model(this, "Knuckles", Content, null, modelPos, modelRotation, modelScale);
			initKnife();

			Mouse.SetPosition(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
			originalMouseState = Mouse.GetState();
		}

		private Vector3 GetStrafe(float yawOffset)
		{
			float rad_yaw = -MathHelper.ToRadians(yawOffset);
			Vector3 transformed = Vector3.Transform(dir, Matrix.CreateFromYawPitchRoll(rad_yaw, 0, 0));
			return new Vector3(transformed.X, 0, transformed.Z);
		}

		Vector3 velocity = new Vector3(0, 0, 0);
		MouseState originalMouseState;
		float yvel = 0;
		bool shot = false;

		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			const float speed = 5;

			velocity = new Vector3(0, 0, 0);

			KeyboardState keyboard = Keyboard.GetState();

			if (keyboard.IsKeyDown(Keys.W))
				velocity += dir;
			if (keyboard.IsKeyDown(Keys.S))
				velocity -= dir;
			if (keyboard.IsKeyDown(Keys.A))
				velocity += GetStrafe(-90);
			if (keyboard.IsKeyDown(Keys.D))
				velocity += GetStrafe(90);

			Vector3 moved = Vector3.Zero;

			if (keyboard.IsKeyDown(Keys.Up))
				moved += knuckles.Direction;
			if (keyboard.IsKeyDown(Keys.Down))
				moved -= knuckles.Direction;
			moved.Y = 0;
			moved.Normalize();
			if (!float.IsNaN(moved.X) && !float.IsNaN(moved.Y) && !float.IsNaN(moved.Z))
				knuckles.Translate(moved * 0.1f);
			if (keyboard.IsKeyDown(Keys.H) && knuckles.Position.Y == 0f)
				yvel = 5;
			knuckles.Translate(Vector3.UnitY * yvel * (float)gameTime.ElapsedGameTime.TotalSeconds);
			yvel -= 0.35f;
			if (knuckles.Position.Y < 0)
			{
				knuckles.Position = new Vector3(knuckles.Position.X, 0, knuckles.Position.Z);
				yvel = 0;
			}

			if (keyboard.IsKeyDown(Keys.Left))
				knuckles.Move(0.1f, test.Direction.Left);
			if (keyboard.IsKeyDown(Keys.Right))
				knuckles.Move(0.1f, test.Direction.Right);

			if (keyboard.IsKeyDown(Keys.Space))
			{
				velocity.Y += speed * 0.2f;
			}
			if (keyboard.IsKeyDown(Keys.LeftControl))
				velocity.Y -= speed * 0.2f;

			Vector3 rotation = knuckles.Rotation;
			if (keyboard.IsKeyDown(Keys.I))
				rotation.X += (float)gameTime.ElapsedGameTime.TotalSeconds * 100;
			if (keyboard.IsKeyDown(Keys.J))
				rotation.Y += (float)gameTime.ElapsedGameTime.TotalSeconds * 100;
			if (keyboard.IsKeyDown(Keys.K))
				rotation.X -= (float)gameTime.ElapsedGameTime.TotalSeconds * 100;
			if (keyboard.IsKeyDown(Keys.L))
				rotation.Y -= (float)gameTime.ElapsedGameTime.TotalSeconds * 100;
			knuckles.Rotation = rotation;

			pos += velocity * (float)gameTime.ElapsedGameTime.TotalSeconds * speed;

			MouseState mouse = Mouse.GetState();
			if (mouse != originalMouseState)
			{
				float dx = originalMouseState.X - mouse.X;
				float dy = originalMouseState.Y - mouse.Y;
				const int inverseY = 1;
				const int inverseX = -1;
				pitch += dy * (float)gameTime.ElapsedGameTime.TotalSeconds * speed * inverseY;
				yaw += dx * (float)gameTime.ElapsedGameTime.TotalSeconds * speed * inverseX;
				//originalMouseState = mouse;
				Mouse.SetPosition(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);
			}
			//if (keyboard.IsKeyDown(Keys.Up))
			//    pitch += 2;
			//if (keyboard.IsKeyDown(Keys.Down))
			//    pitch -= 2;
			//if (keyboard.IsKeyDown(Keys.Right))
			//    yaw += 2;
			//if (keyboard.IsKeyDown(Keys.Left))
			//    yaw -= 2;

			//Rolling mechanics for Camera (dont forget to add rotation to the character!)
			if (keyboard.IsKeyDown(Keys.Q) && roll < 30 && roll >= 0)
				roll += (float)gameTime.ElapsedGameTime.TotalSeconds * 90;
			if (keyboard.IsKeyDown(Keys.E) && roll > -30 && roll <= 0)
				roll -= (float)gameTime.ElapsedGameTime.TotalSeconds * 90;

			//This will get the camera back to normal
			if (roll < 0 && !keyboard.IsKeyDown(Keys.E))
				roll += (float)gameTime.ElapsedGameTime.TotalSeconds * 90;
			if (roll > 0 && !keyboard.IsKeyDown(Keys.Q))
				roll -= (float)gameTime.ElapsedGameTime.TotalSeconds * 90;

			yaw %= 360;
			pitch %= 360;
			roll %= 360;

			if (keyboard.IsKeyDown(Keys.NumPad0))
			{
				knife.Dismount();
				shot = true;
			}
			if (keyboard.IsKeyDown(Keys.NumPad1))
			{
				initKnife();
				shot = false;
			}
			if (shot)
				knife.Move(0.1f, test.Direction.Backwards);

			base.Update(gameTime);
		}


		public void DrawModel(GameTime gameTime, test.Model model)
		{
			Matrix[] transforms = new Matrix[model.XnaModel.Bones.Count];
			model.XnaModel.CopyAbsoluteBoneTransformsTo(transforms);

			foreach (ModelMesh mesh in model.XnaModel.Meshes)
			{
				foreach (BasicEffect effectT in mesh.Effects)
				{
					effectT.EnableDefaultLighting();

					effectT.View = view;
					effectT.Projection = projection;
					effectT.World = transforms[mesh.ParentBone.Index] * model.WorldMatrix;
				}
				mesh.Draw();
			}
		}

		protected override void Draw(GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

			Setup3D();
			floor.Draw(view, projection, effect);
			DrawModel(gameTime, knuckles);
			DrawModel(gameTime, knife);

			Setup2D();
			spriteBatch.Begin();
			spriteBatch.DrawString(debugFont, knife.Position.ToString(), new Vector2(10, 10), Color.White);
			spriteBatch.End();

			base.Draw(gameTime);
		}

		public void Setup3D()
		{
			GraphicsDevice.BlendState = BlendState.AlphaBlend;
			GraphicsDevice.DepthStencilState = DepthStencilState.Default;
			GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
			GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
		}

		public void Setup2D()
		{
			GraphicsDevice.BlendState = BlendState.AlphaBlend;
			GraphicsDevice.DepthStencilState = DepthStencilState.None;
			GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
			GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
		}

	}
}
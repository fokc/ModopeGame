NOTE:
When writing code for the core project, use the 'internal' access modifier as much as possible.
Use only 'public' in places needed for modules using the api, and 'internal' in any other place.
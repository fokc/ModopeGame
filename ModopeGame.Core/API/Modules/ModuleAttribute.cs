using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModopeGame.API.Modules {
    [AttributeUsage(AttributeTargets.Class)]
    public class ModuleAttribute : Attribute {

        private string id;
        public string Id {
            get { return id; }
            internal set { id = value; }
        }

        private string name;
        public string Name {
            get { return name; }
            internal set { name = value; }
        }

        private Module main;
        public Module Main {
            get { return main; }
            internal set { main = value; }
        }

        private string version;
        public string Version {
            get { return version; }
            internal set { version = value; }
        }

        private string author = null;
        public string Author {
            get { return author; }
            internal set { author = value; }
        }

        private string website = null;
        public string Website {
            get { return website; }
            internal set { website = value; }
        }

        private string description = null;
        public string Description {
            get { return description; }
            internal set { description = value; }
        }

        private string documentation = null;
        public string Documentation {
            get { return documentation; }
            internal set { documentation = value; }
        }

        private string[] dependencies;
        public string[] Dependencies {
            get { return dependencies; }
            internal set { dependencies = value; }
        }

        public ModuleAttribute(string id, string name, string version,
                          params string[] dependencies) {
            this.id = id;
            this.name = name;
            this.version = version;
            this.dependencies = dependencies;
        }

        public override string ToString() {
            string toString = "ModuleInfo["
                + "\n  Id:           " + id
                + "\n  Name:         " + name
                + "\n  Main:         " + main.GetType().FullName
                + (version != null ? "\n  Version:      " + version : "")
                + (website != null ? "\n  Website:      " + website : "")
                + (author != null ? "\n  Author:       " + author : "")
                + "\n  Dependencies: " + Debugger.ObjectToString(dependencies).Substring("String".Length)
                + "\n]";
            return toString;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModopeGame.API.Modules {
    /// <summary>
    /// A basic module class.
    /// </summary>
    public abstract class Module {
        /// <summary>
        /// A reference to the engine's core.
        /// </summary>
        private Core core;
        /// <summary>
        /// A reference to the engine's core.
        /// </summary>
        public Core Core {
			get { return core; }
			internal set { core = value; }
        }

        /// <summary>
        /// Basic information about the module.
        /// </summary>
        private ModuleAttribute moduleInfo;
        /// <summary>
        /// Basic information about the module.
        /// </summary>
        public ModuleAttribute Info {
			get { return moduleInfo; }
			internal set { moduleInfo = value; }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ModopeGame.API.Events;

namespace ModopeGame.API.Modules {
    /// <summary>
    /// The main class for managing module loading,activating and handling.
    /// <para>
    /// Unloading modules is not going to be available until future implementation.
    /// </para>
    /// </summary>
	public class ModuleManager {
        /// <summary>
        /// A list containing the loaded modules.
        /// </summary>
        internal List<Module> loadedModules = new List<Module>();

        /// <summary>
        /// Attempts to load all the modules in the specified folder.
        /// </summary>
        /// <param name="directory">The directory containing the modules.</param>
		internal void LoadModules(string directory = "./modules/") {
            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);                                     // Make sure the module directory exists.
            List<Assembly> assemblies
                = Directory.GetFiles(directory, "*.dll").Select(f => Assembly.LoadFrom(Path.GetFullPath(f))).ToList();  // A list containing all assemblies loaded from the directory.
            assemblies.Add(Assembly.GetExecutingAssembly());
            string loadDebugMessage = "Found " + assemblies.Count + " dll files!\n";
            foreach (Assembly assembly in assemblies) {                                                                 // Iterate through the assemblies:
                try {
                    Type[] types = assembly.GetTypes();
                    bool moduleFound = false;
                    foreach (Type type in types) {
                        ModuleAttribute moduleInfo = (ModuleAttribute)Attribute.GetCustomAttribute(
                            type,
                            typeof(ModuleAttribute)
                        );
                        object instance = null;
                        if (moduleInfo != null && typeof(Module).IsAssignableFrom(type) && !moduleFound) {
                            Module module = (Module)assembly.CreateInstance(type.FullName);
                            instance = module;
                            module.Core = Core.Instance;
                            moduleInfo.Main = module;
                            module.Info = moduleInfo;
                            loadedModules.Add(module);
                            moduleFound = true;
                        }

                        EventHandlerAttribute eventHandlerInfo = (EventHandlerAttribute)Attribute.GetCustomAttribute(
                            type,
                            typeof(EventHandlerAttribute)
                        );
                        if (eventHandlerInfo != null) {
                            foreach (MethodInfo method in type.GetMethods()) {
                                EventHandlerAttribute methodEventHandlerInfo = (EventHandlerAttribute)Attribute.GetCustomAttribute(
                                    method,
                                    typeof(EventHandlerAttribute)
                                );
                                if (methodEventHandlerInfo != null) {
                                    if(instance == null && !method.IsStatic && !type.IsAbstract)
                                        assembly.CreateInstance(type.FullName);
                                    var methodStruct = new EventManager.EventMethodStruct {
                                        classInstance = instance,
                                        method = method
                                    };
                                    Core.Instance.EventManager.events.Add(methodStruct);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    Console.WriteLine(e);
                    Debugger.Debug(e, 10);
                }
            }
            loadDebugMessage += "Loaded " + loadedModules.Count + " modules!";
            Debugger.Debug(loadDebugMessage);
        }
    }
}

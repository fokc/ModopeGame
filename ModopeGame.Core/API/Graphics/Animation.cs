﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Interfaces;

namespace ModopeGame.API.Graphics {
    public class Animation : Updateable {

        public Frame[] Frames {
            get;
            set;
        }

        public int CurrentFrameIndex {
            get;
            set;
        }

        public Frame CurrentFrame {
            get {
                return Frames[CurrentFrameIndex % Frames.Length];
            }
        }

        public Animation(Texture2D[] frames, TimeSpan[] durations) : this(frames.Zip(durations, (frame, duration) => new Frame(frame, duration)).ToArray()) { }

        public Animation(params Frame[] frames) {
            this.Frames = frames;
            this.CurrentFrameIndex = 0;
        }

        private TimeSpan start = TimeSpan.MinValue;
        public void Start() {
            if (Core.Instance.gameTime != null)
                start = Core.Instance.gameTime.TotalGameTime;
            else
                start = TimeSpan.Zero;
        }

        public void Update(UpdateEvent e) {
            if (start == null)
                return;
            if ((e.GameTime.TotalGameTime - start) >= CurrentFrame.Duration) {
                CurrentFrameIndex++;
                start = start + CurrentFrame.Duration;
            }
        }
    }

    public class Frame {
        
        public Texture2D Texture {
            get;
            set;
        }

        public TimeSpan Duration {
            get;
            set;
        }

        public Frame(Texture2D texture, TimeSpan duration) {
            this.Texture = texture;
            this.Duration = duration;
        }
    }
}

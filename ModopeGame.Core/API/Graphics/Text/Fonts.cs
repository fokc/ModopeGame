﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace ModopeGame.API.Graphics.Text {

    /// <summary>
    /// A static class containing a bunch of pre-made fonts.
    /// </summary>
    public static class Fonts {

        /// <summary>
        /// Family: Consolas
        /// Size:   8
        /// Style:  Regular
        /// </summary>
        public static SpriteFont DebugFont = Core.Instance.Content.Load<SpriteFont>("Resources/consolas8px.xnb");
    }
}

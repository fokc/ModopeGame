﻿using System;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame.API.Graphics.Text;

namespace ModopeGame.API.Graphics {

    /// <summary>
    /// The canvas class is an extension to the SpriteBatch class, and adds new 2D drawing functions.
    /// </summary>
    public class Canvas : SpriteBatch {

        /// <summary>
        /// A pixel Texture2D object.
        /// Redefined here for quicker access.
        /// </summary>
        public Texture2D Pixel = CommonResources.Pixel;

        /// <summary>
        /// Constructor.
        /// Calls base constructor of SpriteBatch.
        /// </summary>
        public Canvas(GraphicsDevice graphicsDevice) : base(graphicsDevice) {
        }

        /// <summary>
        /// Sets up Graphics Device for 3D rendering.
        /// </summary>
        public void Setup3D() {
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
        }

        /// <summary>
        /// Sets up Graphics Device for 2D rendering.
        /// </summary>
        public void Setup2D() {
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;
        }

        /// <summary>
        /// Draws a line.
        /// </summary>
        /// <param name="start">Line's start position on screen.</param>
        /// <param name="end">Line's start position on screen.</param>
        /// <param name="color">The color of the line.</param>
        /// <param name="width">The width of the line in pixels.</param>
        public void DrawLine(Vector2 start, Vector2 end, Color color, int width = 1) {
            //https://gamedev.stackexchange.com/questions/26013/drawing-a-texture-line-between-two-vectors-in-xna-wp7
            Vector2 edge = end - start;
            float angle = (float)Math.Atan2(edge.Y, edge.X);
            Draw(Pixel,
                 new Rectangle((int)start.X, (int)start.Y, (int)edge.Length(), width),
                 null, color, angle, Vector2.Zero, SpriteEffects.None, 0);

        }

        public new void Draw(Texture2D texture, Vector2 position, Color color) {
            base.Draw(texture, position, null, color, 0f, Vector2.Zero, 1f, SpriteEffects.FlipVertically, 0f);
        }

        [Obsolete("In future versions this method can be removed.")]
        public new void Draw(Texture2D texture, Vector2? position = null, Rectangle? destinationRectangle = null, Rectangle? sourceRectangle = null, Vector2? origin = null, float rotation = 0f, Vector2? scale = null, Color? color = null, SpriteEffects effects = SpriteEffects.None, float layerDepth = 0f) {
            SpriteEffects newEffects =
                (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                |
                (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.Draw(texture, position, destinationRectangle, sourceRectangle, origin, rotation, scale, color, newEffects, layerDepth);
        }

        public new void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth) {
            SpriteEffects newEffects =
                            (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                            |
                            (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.Draw(texture, position, sourceRectangle, color, rotation, origin, scale, newEffects, layerDepth);
        }

        public new void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth) {
            SpriteEffects newEffects =
                            (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                            |
                            (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.Draw(texture, position, sourceRectangle, color, rotation, origin, scale, newEffects, layerDepth);
        }

        public new void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, SpriteEffects effects, float layerDepth) {
            SpriteEffects newEffects =
                            (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                            |
                            (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.Draw(texture, destinationRectangle, sourceRectangle, color, rotation, origin, newEffects, layerDepth);
        }

        public new void Draw(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color) {
            base.Draw(texture, position, sourceRectangle, color, 0f, Vector2.Zero, 1f, SpriteEffects.FlipVertically, 0f);
        }

        public new void Draw(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color) {
            base.Draw(texture, destinationRectangle, sourceRectangle, color, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0f);
        }

        public new void Draw(Texture2D texture, Rectangle destinationRectangle, Color color) {
            base.Draw(texture, destinationRectangle, null, color, 0f, Vector2.Zero, SpriteEffects.FlipVertically, 0f);
        }

        public new void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color, float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth) {
            SpriteEffects newEffects =
                            (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                            |
                            (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.DrawString(spriteFont, text, position, color, rotation, origin, scale, newEffects, layerDepth);
        }

        public new void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color) {
            base.DrawString(spriteFont, text, position, color, 0f, Vector2.Zero, 1f, SpriteEffects.FlipVertically, 0f);
        }

        public new void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color, float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth) {
            SpriteEffects newEffects =
                            (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                            |
                            (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.DrawString(spriteFont, text, position, color, rotation, origin, scale, newEffects, layerDepth);
        }

        public new void DrawString(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color, float rotation, Vector2 origin, Vector2 scale, SpriteEffects effects, float layerDepth) {
            SpriteEffects newEffects =
                            (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                            |
                            (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.DrawString(spriteFont, text, position, color, rotation, origin, scale, newEffects, layerDepth);
        }

        public new void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color) {
            base.DrawString(spriteFont, text, position, color, 0f, Vector2.Zero, 1f, SpriteEffects.FlipVertically, 0f);
        }

        public new void DrawString(SpriteFont spriteFont, string text, Vector2 position, Color color, float rotation, Vector2 origin, float scale, SpriteEffects effects, float layerDepth) {
            SpriteEffects newEffects =
                            (effects.HasFlag(SpriteEffects.FlipHorizontally) ? SpriteEffects.FlipHorizontally : SpriteEffects.None)
                            |
                            (effects.HasFlag(SpriteEffects.FlipVertically) ? SpriteEffects.None : SpriteEffects.FlipVertically);
            base.DrawString(spriteFont, text, position, color, rotation, origin, scale, newEffects, layerDepth);
        }
    }
}

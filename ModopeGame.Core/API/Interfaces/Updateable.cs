using System;
using ModopeGame.API.Events.Core;

namespace ModopeGame.API.Interfaces {
    public interface Updateable {
        void Update(UpdateEvent e);
    }
}

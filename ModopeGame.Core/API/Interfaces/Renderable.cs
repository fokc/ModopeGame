using System;
using ModopeGame.API.Events.Core;

namespace ModopeGame.API.Interfaces {
    public interface Renderable {
        void Render(RenderEvent e);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using ModopeGame.API.Enviroments.Enviroment2D.Physics;
using ModopeGame.API.Events.Core;
using VelcroPhysics.Collision.ContactSystem;
using VelcroPhysics.Collision.Narrowphase;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Utilities;

namespace ModopeGame.API.Enviroments.Enviroment2D {
    public class World2D {

        public List<Enviroment2DObject> Objects {
            get;
            set;
        }

        public World PhysicsWorld {
            get;
            private set;
        }

        /// <summary>
        /// Create a World2D without physics.
        /// </summary>
        public World2D() {
            this.Objects = new List<Enviroment2DObject>();
            this.PhysicsWorld = null;
        }

        /// <summary>
        /// Creates a World2D with default VelcroPhysics physics.
        /// </summary>
        public World2D(Vector2 gravity) : this() {
            ConvertUnits.SetDisplayUnitToSimUnitRatio(100f);
            this.PhysicsWorld = new World(gravity);
            this.PhysicsWorld.ContactManager.PreSolve += (Contact contact, ref Manifold oldManifold) => {
                if (contact.Manifold.PointCount > 0) {
                    Body[] bodies = { contact.FixtureA.Body, contact.FixtureB.Body };
                    Enviroment2DObject[] bodiesUserData = bodies.Where(body => body.UserData is Enviroment2DObject)
                                                                .Select(body => body.UserData as Enviroment2DObject)
                                                                .ToArray();
                    if (bodiesUserData.Length >= 2) {
                        CollisionEvent e = new CollisionEvent(this, bodiesUserData[0], bodiesUserData[1], contact.Manifold);
                        Core.Instance.EventManager.CallEvent(e);
                        if(e.Canceled)
                            contact.Manifold = new Manifold();
                    }
                }
            };
        }

        public void Update(UpdateEvent e) {
            Objects.ForEach(obj => obj.Update(e));
            if (this.PhysicsWorld != null)
                this.PhysicsWorld.Step((float)e.GameTime.ElapsedGameTime.TotalSeconds);
        }
    }
}

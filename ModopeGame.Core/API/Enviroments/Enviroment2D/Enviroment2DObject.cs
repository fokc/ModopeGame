using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ModopeGame.API.Enviroments.Enviroment2D.Physics;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Interfaces;
using VelcroPhysics.Dynamics;

namespace ModopeGame.API.Enviroments.Enviroment2D {
    public abstract class Enviroment2DObject : Updateable {

        public Body Body {
            get;
            set;
        }

        public Vector2 Position {
            get {
                return Body.Position.ToDisplayUnits();
            }
            set {
                Body.Position = value.ToSimUnits();
            }
        }

        public Vector2 Velocity {
            get {
                return Body.LinearVelocity.ToDisplayUnits();
            }
            set {
                Body.LinearVelocity = value.ToSimUnits();
            }
        }

        private List<string> _tags;
        public List<string> Tags {
            get {
                if (_tags == null)
                    _tags = new List<string>();
                return _tags;
            }
        }

        public abstract void Update(UpdateEvent e);
    }
}

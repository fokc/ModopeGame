﻿using System;
using Microsoft.Xna.Framework;
using VelcroPhysics.Utilities;

namespace ModopeGame.API.Enviroments.Enviroment2D.Physics {
    public static class PhysicsUtilities {

        public static Vector2 ToSimUnits(this Vector2 vector) {
            return ConvertUnits.ToSimUnits(vector);
        }

        public static float ToSimUnits(this int value) {
            return ConvertUnits.ToSimUnits(value);
        }

        public static float ToSimUnits(this float value) {
            return ConvertUnits.ToSimUnits(value);
        }

        public static float ToSimUnits(this double value) {
            return ConvertUnits.ToSimUnits(value);
        }

        public static Vector2 ToDisplayUnits(this Vector2 vector) {
            return ConvertUnits.ToDisplayUnits(vector);
        }

        public static float ToDisplayUnits(this int value) {
            return ConvertUnits.ToDisplayUnits(value);
        }

        public static float ToDisplayUnits(this float value) {
            return ConvertUnits.ToDisplayUnits(value);
        }
    }
}

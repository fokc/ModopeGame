﻿using System;
using Microsoft.Xna.Framework;
using ModopeGame.API.Events;
using VelcroPhysics.Collision.Narrowphase;

namespace ModopeGame.API.Enviroments.Enviroment2D.Physics {

    public class CollisionEvent : Event, Cancelable {

        public readonly Enviroment2DObject First;
        public readonly Enviroment2DObject Second;
        public readonly World2D World;
        public readonly Manifold Manifold;

        public CollisionEvent(World2D world, Enviroment2DObject first, Enviroment2DObject second, Manifold manifold) {
            this.World = world;
            this.First = first;
            this.Second = second;
            this.Manifold = manifold;
        }

        /// <summary>
        /// Canceling will result the colliding objects ignoring each other and potentialy going through one another.
        /// </summary>
        public bool Canceled {
            get;
            set;
        }

        public float CollisionAngle {
            get {
                Vector2 normal = -this.Manifold.LocalNormal;
                return MathHelper.ToDegrees((float)Math.Atan2(normal.Y, normal.X));
            }
        }
    }
}

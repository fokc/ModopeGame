using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame.API.Enviroments.Enviroment2D.Physics;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Graphics;
using ModopeGame.API.Interfaces;
using VelcroPhysics.Collision.Shapes;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Factories;

namespace ModopeGame.API.Enviroments.Enviroment2D.Presets {
    public class LineObject : Enviroment2DObject, Renderable2D {

        public Vector2 Start {
            get {
                return ((EdgeShape)Body.FixtureList[0].Shape).Vertex1.ToDisplayUnits();
            }
            set {
                ((EdgeShape)Body.FixtureList[0].Shape).Vertex1 = value.ToSimUnits();
            }
        }

        public Vector2 End {
            get {
                return ((EdgeShape)Body.FixtureList[0].Shape).Vertex2.ToDisplayUnits();
            }
            set {
                ((EdgeShape)Body.FixtureList[0].Shape).Vertex1 = value.ToSimUnits();
            }
        }

        public Animation Animation {
            get {
                return null;
            }
        }

        public Vector2 Origin {
            get {
                return Vector2.Zero;
            }
        }

        public LineObject(World2D world, Vector2 start, Vector2 end, float friction = 0.6f, params string[] tags) {
            Body = BodyFactory.CreateEdge(
                world.PhysicsWorld,
                start.ToSimUnits(),
                end.ToSimUnits(),
                this
            );
            Body.BodyType = BodyType.Kinematic;
            Body.Friction = friction;
            world.Objects.Add(this);
            this.Tags.AddRange(tags);
        }

        public void Render(RenderEvent e) {
            e.Canvas.DrawLine(this.Start, this.End, Color.White);
        }

        public override void Update(UpdateEvent e) {}
    }
}

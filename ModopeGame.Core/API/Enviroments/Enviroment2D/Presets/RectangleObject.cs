﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame.API.Enviroments.Enviroment2D.Physics;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Graphics;
using ModopeGame.API.Interfaces;
using VelcroPhysics.Dynamics;
using VelcroPhysics.Factories;

using XnaRectangle = Microsoft.Xna.Framework.Rectangle;

namespace ModopeGame.API.Enviroments.Enviroment2D.Presets {
    public class RectangleObject : Enviroment2DObject, Renderable2D {

        public float Width {
            get;
            private set;
        }

        public float Height {
            get;
            private set;
        }

        public Animation Animation {
            get;
            set;
        }

        public Vector2 Origin {
            get {
                return new Vector2(Width / 2, Height / 2);
            }
        }

        public RectangleObject(World2D world, float width, float height, Vector2 position, Animation animation, float friction = 0.6f, params string[] tags) {
            this.Width = width;
            this.Height = height;
            Body = BodyFactory.CreateRectangle(
                world.PhysicsWorld,
                width.ToSimUnits(),
                height.ToSimUnits(),
                1f,
                position.ToSimUnits(),
                0f,
                BodyType.Dynamic,
                this
            );
            Body.Friction = friction;
            Body.FixedRotation = true;
            Position = position;
            Velocity = Vector2.Zero;
            this.Animation = animation;

            world.Objects.Add(this);
            this.Tags.AddRange(tags);
        }

        public void Render(RenderEvent e) {
            e.Canvas.Draw(this.Animation.CurrentFrame.Texture, this.Position, null, Color.White, this.Body.Rotation, this.Origin, 1f, SpriteEffects.None, 0f);
        }

        public override void Update(UpdateEvent e) {
            this.Animation.Update(e);
        }
    }
}

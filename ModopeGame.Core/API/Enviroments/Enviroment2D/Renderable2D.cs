﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Graphics;
using ModopeGame.API.Interfaces;

namespace ModopeGame.API.Enviroments.Enviroment2D {
    public interface Renderable2D : Renderable {
        Animation Animation {
            get;
        }

        Vector2 Origin {
            get;
        }
    }
}

﻿using System;
using Microsoft.Xna.Framework;
using ModopeGame.API.Events.Core;

namespace ModopeGame.API.Enviroments.Enviroment3D {

    public class Camera : Model {

        public Matrix View {
            get {
                float rad_yaw = MathHelper.ToRadians(this.Rotation.Y);
                float rad_pitch = MathHelper.ToRadians(this.Rotation.X);
                float rad_roll = MathHelper.ToRadians(this.Rotation.Z);

                Matrix worldMatrix = Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll)
                                           * Matrix.CreateTranslation(this.Position)
                                           * (this.Parent != null ? 
                                              this.Parent.WorldMatrix :
                                              Matrix.Identity);

                Vector3 translation = worldMatrix.Translation;
                Vector3 forward = worldMatrix.Forward;
                Vector3 up = worldMatrix.Down;

                return Matrix.CreateLookAt(translation, translation + forward, up);
            }
        }

        public Camera(Vector3 position = default(Vector3), Vector3 direction = default(Vector3))
            : base(default(string), null, position, direction) { }
    }
}
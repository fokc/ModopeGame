﻿using System;
using Microsoft.Xna.Framework;

namespace ModopeGame.API.Enviroments.Enviroment3D {
    
    public static class Utilities {
        
        public static Vector3 QuaternionToEuler(this Quaternion q) {
            Vector3 v = new Vector3();

            v.X = (float)Math.Atan2(
                2 * q.Y * q.W - 2 * q.X * q.Z,
                1 - 2 * Math.Pow(q.Y, 2) - 2 * Math.Pow(q.Z, 2)
            );

            v.Y = (float)Math.Asin(
                2 * q.X * q.Y + 2 * q.Z * q.W
            );

            v.Z = (float)Math.Atan2(
                2 * q.X * q.W - 2 * q.Y * q.Z,
                1 - 2 * Math.Pow(q.X, 2) - 2 * Math.Pow(q.Z, 2)
            );

            if (q.X * q.Y + q.Z * q.W == 0.5) {
                v.X = (float)(2 * Math.Atan2(q.X, q.W));
                v.Z = 0;
            } else if (q.X * q.Y + q.Z * q.W == -0.5) {
                v.X = (float)(-2 * Math.Atan2(q.X, q.W));
                v.Z = 0;
            }

            return v;
        }
    }
}

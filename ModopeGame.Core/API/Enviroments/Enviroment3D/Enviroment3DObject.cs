﻿using System;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Interfaces;
using VelcroPhysics.Dynamics;
namespace ModopeGame.API.Enviroments.Enviroment3D {
    public abstract class Enviroment3DObject : Updateable {
        
        public abstract void Update(UpdateEvent e);
    }
}

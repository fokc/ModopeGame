﻿using System;
using Microsoft.Xna.Framework;
using ModopeGame.API.Events;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Interfaces;
using ModopeGame.Content;

using XnaModel = Microsoft.Xna.Framework.Graphics.Model;

namespace ModopeGame.API.Enviroments.Enviroment3D {

    public class Model : Enviroment3DObject {

        #region Properties
        public XnaModel XnaModel {
            get;
            set;
        }

        public Vector3 Position { // X position, Y position, Z position
            get;
            set;
        }
        public Vector3 Rotation { // X rotation, Y rotation, Z rotation
            get;
            set;
        }
        public Vector3 Scale { // X scale, Y scale, Z scale
            get;
            set;
        }

        public Model Parent {
            get;
            set;
        }

        private Matrix _worldMatrix = default(Matrix);
        private bool resetWorldMatrix = true;

        public Matrix WorldMatrix {
            get {
                if (resetWorldMatrix) {
                    float rad_yaw = MathHelper.ToRadians(this.Rotation.Y);
                    float rad_pitch = MathHelper.ToRadians(this.Rotation.X);
                    float rad_roll = MathHelper.ToRadians(this.Rotation.Z);

                    Matrix world = Matrix.CreateScale(this.Scale)
                                         * Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll)
                                         * Matrix.CreateTranslation(this.Position);
                    resetWorldMatrix = false;
                    _worldMatrix = world;
                }
                if (this.Parent != null)
                    return _worldMatrix * this.Parent.WorldMatrix;
                return _worldMatrix;
            }
        }

        public override void Update(UpdateEvent e) {
            resetWorldMatrix = true;
        }

        public Vector3 Direction {
            get {
                return WorldMatrix.Forward;
            }
        }
        #endregion

        #region Constructors
        public Model(XnaModel model, Model parent = null,
                     Vector3 position = default(Vector3), Vector3 rotation = default(Vector3), Vector3 scale = default(Vector3)) {
            this.XnaModel = model;

            this.Parent = parent;
            this.Position = position * (Parent == null ? Vector3.One : Vector3.One / this.Parent.Scale);
            this.Rotation = rotation;
            this.Scale = scale;
        }

        public Model(string path, Model parent = null,
             Vector3 position = default(Vector3), Vector3 rotation = default(Vector3), Vector3 scale = default(Vector3))
            : this(Core.Instance.Content.Load<XnaModel>(path), parent, position, rotation, scale) { }
        #endregion

        //Moves
        public Model Translate(Vector3 translation, bool relative = false) //Move model using Vector3
        {
            if (relative) {
                float rad_yaw = MathHelper.ToRadians(this.Rotation.Y);
                float rad_pitch = MathHelper.ToRadians(this.Rotation.X);
                float rad_roll = MathHelper.ToRadians(this.Rotation.Z);

                translation = Vector3.Transform(translation, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
            }
            this.Position += translation * (Parent == null ? Vector3.One : Vector3.One / this.Parent.Scale);
            return this;
        }

        public Model Translate(float x, float y, float z, bool relative = false) //Move model using Vector3
        {
            return Translate(new Vector3(x, y, z), relative);
        }

        public Model Move(float amount, Enviroment3D.Direction direction = Enviroment3D.Direction.Forwards) {
            Vector3 movement = Vector3.Zero;

            float rad_yaw = MathHelper.ToRadians(this.Rotation.Y);
            float rad_pitch = MathHelper.ToRadians(this.Rotation.X);
            float rad_roll = MathHelper.ToRadians(this.Rotation.Z);
            Matrix directionMatrix = Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll);

            if (direction == (Enviroment3D.Direction.Forwards))
                movement -= directionMatrix.Forward * amount;
            if (direction == (Enviroment3D.Direction.Backwards))
                movement -= directionMatrix.Backward * amount;
            if (direction == (Enviroment3D.Direction.Left))
                movement += directionMatrix.Left * amount;
            if (direction == (Enviroment3D.Direction.Right))
                movement += directionMatrix.Right * amount;
            if (direction == (Enviroment3D.Direction.Up))
                movement += directionMatrix.Up * amount;
            if (direction == (Enviroment3D.Direction.Down))
                movement += directionMatrix.Down * amount;
            return Translate(movement);
        }

        public void Dismount() {
            if (this.Parent == null)
                return;

            this.Scale = this.WorldMatrix.Scale;
            this.Rotation = this.WorldMatrix.Rotation.QuaternionToEuler();
            this.Position = this.WorldMatrix.Translation;
            this.Parent = null;
        }
    }


    public enum Direction {
        Forwards,
        Backwards,
        Up,
        Down,
        Left,
        Right
    }

}
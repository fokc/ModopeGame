﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame.API.Events;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Interfaces;

namespace ModopeGame.API.Enviroments.Enviroment3D {
    public class Projector : Updateable {

        public float FOV {
            get;
            set;
        }

        public Vector2 Resolution {
            get;
            set;
        }

        public float Near {
            get;
            set;
        }

        public float Far {
            get;
            set;
        }

        public Projector(float FOV, Vector2 resolution, float far, float near) {
            this.FOV = FOV;
            this.Resolution = resolution;
            this.Far = far;
            this.Near = near;
        }

        private Matrix _projectionMatrix = default(Matrix);
        private bool resetProjectionMatrix = true;

        public Matrix ProjectionMatrix {
            get {
                if (resetProjectionMatrix) {
                    _projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(FOV), Resolution.X / Resolution.Y, Near, Far);
                    resetProjectionMatrix = false;
                }
                return _projectionMatrix;
            }
        }

        public void DrawModel(Model model, Camera camera) {
            if (model == null)
                return;
            Matrix[] transforms = new Matrix[model.XnaModel.Bones.Count];
            model.XnaModel.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in model.XnaModel.Meshes) {
                foreach (BasicEffect effectT in mesh.Effects) {
                    effectT.EnableDefaultLighting();

                    effectT.View = camera.View;
                    effectT.Projection = ProjectionMatrix;
                    effectT.World = transforms[mesh.ParentBone.Index] * model.WorldMatrix;
                }
                mesh.Draw();
            }
        }

        public void DrawModels(Model[] models, Camera camera) {
            DrawModels(models.ToList(), camera);
        }

        public void DrawModels(List<Model> models, Camera camera) {
            models.ForEach(model => DrawModel(model, camera));
        }

        public void DrawModels(Enviroment3DObject[] objects, Camera camera) {
            DrawModels(objects.Where(obj => obj is Model).Select(obj => (Model)obj).ToArray(), camera);
        }

        public void DrawModels(List<Enviroment3DObject> objects, Camera camera) {
            DrawModels(objects.Where(obj => obj is Model).Select(obj => (Model)obj).ToArray(), camera);
        }

        public void Update(UpdateEvent e) {
            resetProjectionMatrix = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Interfaces;

namespace ModopeGame.API.Enviroments.Enviroment3D {
    public class World3D : Updateable {

        public List<Enviroment3DObject> Objects {
            get;
            set;
        }

        public World3D(Projector projector = null) {
            this.Objects = new List<Enviroment3DObject>();
        }

        public void AddObject(Enviroment3DObject obj) {
            Objects.Add(obj);
        }

        public void Update(UpdateEvent e) {
            Objects.ForEach(obj => obj.Update(e));
        }
    }
}

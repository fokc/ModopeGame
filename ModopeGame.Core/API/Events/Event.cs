using System;
namespace ModopeGame.API.Events {

    /// <summary>
    /// An event class. Contains data about the event.
    /// </summary>
    public abstract class Event {}
}

﻿using System;
namespace ModopeGame.API.Events {

    /// <summary>
    /// A cancelable event type.
    /// </summary>
    public interface Cancelable {
        /// <summary>
        /// Whether the event is canceled or not.
        /// </summary>
        bool Canceled {
            get;
            set;
        }
    }
}

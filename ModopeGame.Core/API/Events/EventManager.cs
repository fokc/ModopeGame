﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ModopeGame.API.Events {

    public class EventManager {

        public struct EventMethodStruct {
            public MethodInfo method;
            public object classInstance;
        }

        internal List<EventMethodStruct> events = new List<EventMethodStruct>();

        public void CallEvent(Event e) {
            foreach (EventMethodStruct eventMethodStruct in events) {
                MethodInfo method = eventMethodStruct.method;
                if (method.GetParameters()[0].ParameterType.IsAssignableFrom(e.GetType()))
                    method.Invoke(eventMethodStruct.classInstance, new object[] { e });
            }
        }
    }
}

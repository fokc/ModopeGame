﻿using System;
namespace ModopeGame.API.Events {

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class EventHandlerAttribute : Attribute {

        internal virtual bool CoreEvent {
            get { return false; }
        }

        private float priority;
        public float Priority {
            get { return priority; }
            set {
                if (value < 1000 && !CoreEvent)
                    throw new Exception("Only core events can have a priority lower than 1000!");
                if (value > 9000 && !CoreEvent)
                    throw new Exception("Only core events can have a priority higher than 9000!");
                priority = value;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    internal class CoreEventHandlerAttribute : EventHandlerAttribute {
        internal override bool CoreEvent {
            get { return true; }
        }
    }
}

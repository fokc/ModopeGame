﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Events.Input.Events;
using GameCore = ModopeGame.Core;

namespace ModopeGame.API.Events.Input {

    [EventHandler]
    public static class KeysHandler {

        private static List<Keys> previousKeys;

        [EventHandler]
        public static void Init(InitializationEvent e) {
            previousKeys = new List<Keys>();
            previousKeys.AddRange(Keyboard.GetState().ToKeys());
            previousKeys.AddRange(Mouse.GetState().ToKeys());
        }

        [CoreEventHandler(Priority = 0)]
        public static void HandleInput(UpdateEvent e) {
            List<Keys> keys = new List<Keys>();             // Create keys list
            keys.AddRange(Keyboard.GetState().ToKeys());    // Add keyboard keys
            keys.AddRange(Mouse.GetState().ToKeys());       // Add mouse buttons
            HandleKeys(keys, e.GameTime);                   // Handle keys.
            previousKeys = keys;                            // Update last keys.
        }

        private static void HandleKeys(List<Keys> keys, GameTime gameTime) {
            List<Keys> oldKeys = previousKeys;

            List<Keys> pressed = new List<Keys>(), released = new List<Keys>();

            foreach (Keys key in Enum.GetValues(typeof(Keys))) {
                if (keys.Contains(key) && !oldKeys.Contains(key))
                    pressed.Add(key);
                else if (!keys.Contains(key) && oldKeys.Contains(key))
                    released.Add(key);
            }

            if (pressed.Count > 0)
                GameCore.Instance.EventManager.CallEvent(new KeyPressedEvent(pressed, gameTime));
            if (keys.Count > 0)
                GameCore.Instance.EventManager.CallEvent(new KeyHeldEvent(keys, gameTime));
            if (released.Count > 0)
                GameCore.Instance.EventManager.CallEvent(new KeyReleasedEvent(released, gameTime));
        }
    }
}

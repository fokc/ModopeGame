using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ModopeGame.API.Events.Input.Events {

    /// <summary>
    /// Key held event.
    /// Occures during the game's runtime, when keys are held.
    /// </summary>
    public class KeyHeldEvent : Event {

        public readonly List<Keys> KeysHeld;
        public readonly GameTime GameTime;

        public KeyHeldEvent(List<Keys> keysHeld, GameTime gameTime) {
            KeysHeld = keysHeld;
            GameTime = gameTime;
        }
    }
}

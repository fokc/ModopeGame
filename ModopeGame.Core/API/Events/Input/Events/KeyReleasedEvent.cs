using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace ModopeGame.API.Events.Input.Events {

    /// <summary>
    /// Key released event.
    /// Occures during the game's runtime, when keys are released.
    /// </summary>
    public class KeyReleasedEvent : Event {

        public readonly List<Keys> KeysReleased;
        public readonly GameTime GameTime;

        public KeyReleasedEvent(List<Keys> keysReleased, GameTime gameTime) {
            KeysReleased = keysReleased;
            GameTime = gameTime;
        }
    }
}

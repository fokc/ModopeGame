﻿using System;
using Microsoft.Xna.Framework;

namespace ModopeGame.API.Events.Input.Events {

    /// <summary>
    /// Mouse move event.
    /// Occures during the game's runtime, when the mouse is moved.
    /// </summary>
    public class MouseMoveEvent : Event, Cancelable {
        
        public readonly Vector2 From;
        public readonly Vector2 To;
        public readonly GameTime GameTime;

        public Vector2 Delta {
            get {
                return To - From;
            }
        }

        /// <summary>
        /// Canceling will result the mouse returning to its original position after processing.
        /// </summary>
        public bool Canceled {
            get;
            set;
        }

        public MouseMoveEvent(Vector2 from, Vector2 to, GameTime gameTime) {
            From = from;
            To = to;
            GameTime = gameTime;
        }
    }
}

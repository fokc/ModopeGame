using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ModopeGame.API.Events.Input.Events {

    /// <summary>
    /// Key pressed event.
    /// Occures during the game's runtime, when keys are pressed.
    /// </summary>
    public class KeyPressedEvent : Event {

        public readonly List<Keys> KeysPressed;
        public readonly GameTime GameTime;

        public KeyPressedEvent(List<Keys> keysPressed, GameTime gameTime) {
            KeysPressed = keysPressed;
            GameTime = gameTime;
        }
    }
}

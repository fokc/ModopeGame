﻿using System;
using Microsoft.Xna.Framework.Input;
using ModopeGame.API.Events.Core;

using GameCore = ModopeGame.Core;
using Microsoft.Xna.Framework;
using ModopeGame.API.Events.Input.Events;

namespace ModopeGame.API.Events.Input {

    [EventHandler]
    public static class MouseInputHandler {

        public static Vector2 resetPosition =new Vector2(GameCore.Instance.GraphicsDevice.Viewport.Width / 2, GameCore.Instance.GraphicsDevice.Viewport.Height / 2);
        private static Vector2 previousMousePosition;

        private static bool lastActive = false;

        private static void checkReset() {
            bool active = GameCore.Instance.IsActive;
            if(active && !lastActive)
                Mouse.SetPosition((int)resetPosition.X, (int)resetPosition.Y);
            lastActive = active;
        }


        [CoreEventHandler(Priority = 10000)]
        public static void Init(InitializationEvent e) {
            checkReset();
            MouseState state = Mouse.GetState();
            previousMousePosition = new Vector2(state.X, state.Y);
        }

        [CoreEventHandler(Priority = 0)]
        public static void HandleInput(UpdateEvent e) {
            checkReset();                                           // Check if reseting mouse is needed.
            if (!GameCore.Instance.IsActive)
                return;
            MouseState state = Mouse.GetState();                    // Get current mouse state.
            Vector2 mousePosition = new Vector2(state.X, state.Y);  // Get current mouse position.
            HandleMouse(ref mousePosition, e.GameTime);             // Handle mouse.
            previousMousePosition = mousePosition;                  // Update last mouse position.
        }

        private static void HandleMouse(ref Vector2 mousePosition, GameTime gameTime) {
            Vector2 from = previousMousePosition;
            Vector2 to = mousePosition;
            if ((to - from).Length() > 0) {
                MouseMoveEvent mouseMoveEvent = new MouseMoveEvent(from, to, gameTime);
                GameCore.Instance.EventManager.CallEvent(mouseMoveEvent);
                if(mouseMoveEvent.Canceled) {
                    Mouse.SetPosition((int)from.X, (int)from.Y);
                    mousePosition = from;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

using XnaKeys = Microsoft.Xna.Framework.Input.Keys;

namespace ModopeGame.API.Events.Input {

    public enum Keys {
        
        /****************
         ****************
         ***          ***
         *** Keyboard ***
         ***          ***
         ****************         ****************/

        None,
        Back,
        Tab,
        Enter,
        CapsLock,
        Escape,
        Space,
        PageUp,
        PageDown,
        End,
        Home,
        Left,
        Up,
        Right,
        Down,
        Select,
        Print,
        Execute,
        PrintScreen,
        Insert,
        Delete,
        Help,
        D0,
        D1,
        D2,
        D3,
        D4,
        D5,
        D6,
        D7,
        D8,
        D9,
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,
        LeftWindows,
        RightWindows,
        Apps,
        Sleep,
        NumPad0,
        NumPad1,
        NumPad2,
        NumPad3,
        NumPad4,
        NumPad5,
        NumPad6,
        NumPad7,
        NumPad8,
        NumPad9,
        Multiply,
        Add,
        Separator,
        Subtract,
        Decimal,
        Divide,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        F13,
        F14,
        F15,
        F16,
        F17,
        F18,
        F19,
        F20,
        F21,
        F22,
        F23,
        F24,
        NumLock,
        Scroll,
        LeftShift,
        RightShift,
        LeftControl,
        RightControl,
        LeftAlt,
        RightAlt,
        BrowserBack,
        BrowserForward,
        BrowserRefresh,
        BrowserStop,
        BrowserSearch,
        BrowserFavorites,
        BrowserHome,
        VolumeMute,
        VolumeDown,
        VolumeUp,
        MediaNextTrack,
        MediaPreviousTrack,
        MediaStop,
        MediaPlayPause,
        LaunchMail,
        SelectMedia,
        LaunchApplication1,
        LaunchApplication2,
        OemSemicolon,
        OemPlus,
        OemComma,
        OemMinus,
        OemPeriod,
        OemQuestion,
        OemTilde,
        OemOpenBrackets,
        OemPipe,
        OemCloseBrackets,
        OemQuotes,
        Oem8,
        OemBackslash,
        ProcessKey,
        Attn,
        Crsel,
        Exsel,
        EraseEof,
        Play,
        Zoom,
        Pa1,
        OemClear,
        ChatPadGreen,
        ChatPadOrange,
        Pause,
        ImeConvert,
        ImeNoConvert,
        Kana,
        Kanji,
        OemAuto,
        OemCopy,
        OemEnlW,

        /*************
         *************
         ***       ***
         *** Mouse ***
         ***       ***
         *************
         *************/

        /// <summary>
        /// Mouse Left
        /// </summary>
        Mouse1,
        /// <summary>
        /// Mouse Right
        /// </summary>
        Mouse2,
        /// <summary>
        /// Mouse Middle
        /// </summary>
        Mouse3,
        Mouse4,
        Mouse5,
        /// <summary>
        /// Mouse Wheel Scroll Up
        /// </summary>
        ScrollUp,
        /// <summary>
        /// Mouse Wheel Scroll Down
        /// </summary>
        ScrollDown

    }

    /// <summary>
    /// Utilities for keyboard keys.
    /// </summary>
    public static class KeysUtilities {

        /// <summary>
        /// Constructs a KeyboardKeys enum from KeyboardState object.
        /// </summary>
        public static List<Keys> FromKeyboardState(KeyboardState keyboardState) {
            List<Keys> keys = new List<Keys>();
            foreach (XnaKeys key in keyboardState.GetPressedKeys())
                keys.Add((Keys)Enum.Parse(typeof(Keys), key.ToString()));
            return keys;
        }

        public static List<Keys> FromMouseState(MouseState mouseState) {
            List<Keys> keys = new List<Keys>();
            if (mouseState.LeftButton   == ButtonState.Pressed) keys.Add(Keys.Mouse1);
            if (mouseState.RightButton  == ButtonState.Pressed) keys.Add(Keys.Mouse2);
            if (mouseState.MiddleButton == ButtonState.Pressed) keys.Add(Keys.Mouse3);
            if (mouseState.XButton1     == ButtonState.Pressed) keys.Add(Keys.Mouse4);
            if (mouseState.XButton2     == ButtonState.Pressed) keys.Add(Keys.Mouse5);
            return keys;
        }

        public static List<Keys> ToKeys(this KeyboardState keyboardState) {
            return FromKeyboardState(keyboardState);
        }

        public static List<Keys> ToKeys(this MouseState mouseState) {
            return FromMouseState(mouseState);
        }
    }
}

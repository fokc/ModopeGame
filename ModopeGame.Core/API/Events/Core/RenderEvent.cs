﻿using System;
using Microsoft.Xna.Framework;
using ModopeGame.API.Graphics;

namespace ModopeGame.API.Events.Core {

    /// <summary>
    /// Render event.
    /// Occures every frame.
    /// Use mainly for rendering.
    /// </summary>
    public class RenderEvent : Event {
        public readonly GameTime GameTime;
        public readonly Canvas Canvas;

        public RenderEvent(GameTime gameTime, Canvas canvas) {
            GameTime = gameTime;
            Canvas = canvas;
        }
    }
}

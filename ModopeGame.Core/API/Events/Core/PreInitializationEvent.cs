using System;
namespace ModopeGame.API.Events.Core {
    
    /// <summary>
    /// Pre initialization event.
    /// Occures before initialization event.
    /// Use mainly for initializing varialbes.
    /// 
    /// This event only fires once, immidiatly after all the modules have been loaded.
    /// </summary>
    public class PreInitializationEvent : Event { }
}

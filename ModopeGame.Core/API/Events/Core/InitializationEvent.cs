﻿using System;
namespace ModopeGame.API.Events.Core {
    
    /// <summary>
    /// Initialization event.
    /// Occures during the game initializatoin phase.
    /// Use mainly for general initialization.
    /// 
    /// This event only fires once, during the initialization phase of the game.
    /// </summary>
    public class InitializationEvent : Event { }
}

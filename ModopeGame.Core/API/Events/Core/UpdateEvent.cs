﻿using System;
using Microsoft.Xna.Framework;

namespace ModopeGame.API.Events.Core {

    /// <summary>
    /// Update event.
    /// Occures every game tick.
    /// Use mainly for updating data.
    /// </summary>
    public class UpdateEvent : Event {
        public readonly GameTime GameTime;

        public UpdateEvent(GameTime gameTime) {
            GameTime = gameTime;
        }
    }
}

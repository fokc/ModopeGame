using System;
using ModopeGame.Content;

namespace ModopeGame.API.Events.Core {

    /// <summary>
    /// Load event.
    /// Occures during the game loading phase.
    /// Use mainly for loading assets.
    /// 
    /// This event only fires once, during the initialization phase of the game.
    /// </summary>
    public class LoadEvent : Event {
        public readonly AdvancedContentManager Content;

        public LoadEvent(AdvancedContentManager content) {
            Content = content;
        }
    }
}

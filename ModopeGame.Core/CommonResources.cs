﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace ModopeGame {
    /// <summary>
    /// Static class containing some commonly used objects in the game engine.
    /// </summary>
    public static class CommonResources {

        /// <summary>
        /// A Texture2D object containing only 1 pixel.
        /// </summary>
        internal static Texture2D pixel = null;
        /// <summary>
        /// A Texture2D object containing only 1 pixel.
        /// </summary>
        public static Texture2D Pixel {
            get {
                if (pixel == null) {
                    pixel = new Texture2D(Core.Instance.GraphicsDevice, 1, 1);  // Create a new Texture variable, 1x1 in size.
                    pixel.SetData(new Color[] { Color.White });                 // Sets it's data to [white] (picture = white pixel).
                }
                return pixel;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using ModopeGame.API.Graphics.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ModopeGame.API.Modules;
using ModopeGame.API.Events;
using ModopeGame.API.Graphics;
using ModopeGame.Content;
using ModopeGame.API.Events.Core;

namespace ModopeGame {
    /// <summary>
    /// The core of the game engine.
    /// </summary>
    public class Core : Game {
        /// <summary>
        /// Graphics Device handle. Used mainly for rendering.
        /// </summary>
        public GraphicsDeviceManager graphics;
        /// <summary>
        /// Graphics Device drawing handle. Used for drawing to the screen.
        /// </summary>
        internal Canvas canvas;

        public GameTime gameTime = null;

        /// <summary>
        /// An instance of the game core.
        /// </summary>
        public static Core Instance {
            get;
            private set;    // Only allow for this class to assign value to this property.
        }

        /// <summary>
        /// Content Manager, handles the loading game assets.
        /// </summary>
        public new AdvancedContentManager Content {
            get;
            private set;
        }

        /// <summary>
        /// Event Manager, handles event invoking and handling.
        /// </summary>
        public EventManager EventManager {
            get;
            private set;
        }

        /// <summary>
        /// Module Manager, handles loading and initializing modules.
        /// </summary>
        public ModuleManager ModuleManager {
            get;
            private set;
        }

        internal Vector2 _dimensions = new Vector2(768, 480);
        public Vector2 Dimensions {
            get {
                return _dimensions;
            }
            set {
                _dimensions = value;
                graphics.PreferredBackBufferWidth = (int)_dimensions.X;
                graphics.PreferredBackBufferHeight = (int)_dimensions.Y;
            }
        }

        internal bool _fullscreen;
        public bool Fullscreen {
            get {
                return _fullscreen;
            }
            set {
                _fullscreen = value;
                graphics.IsFullScreen = value;
            }
        }

        public Color BackgroundColor {
            get;
            set;
        }

        /// <summary>
        /// Constructor. Sets core properties for the game engine to work.
        /// </summary>
        public Core() {
            Content = new AdvancedContentManager(base.Content.ServiceProvider); // Change current content manager to an advanced one.
            graphics = new GraphicsDeviceManager(this);                         // Create a new Graphics Device handle.
            EventManager = new EventManager();                                  // Create a new Event Manager.
            ModuleManager = new ModuleManager();                                // Create a module manager.

            Dimensions = _dimensions;
            BackgroundColor = Color.Black;

            Content.RootDirectory = "Content";                                  // Sets the root content director to 'Content' (kind of useless).
            Instance = this;                                                    // Sets the Instance property value to this instance.

            ModuleManager.LoadModules();                                        // Load modules.

            foreach (Module m in ModuleManager.loadedModules)
                Debugger.Debug(m.Info, 10);

            var preInitializationEvent = new PreInitializationEvent();          // Invoke a PreInitializationEvent.
            EventManager.CallEvent(preInitializationEvent);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            // TODO: Add your initialization logic here

            var initializationEvent = new InitializationEvent();    // Invoke a InitializationEvent.
            EventManager.CallEvent(initializationEvent);

             dest = new RenderTarget2D(GraphicsDevice,
                                                             (int)Dimensions.X,
                                                             (int)Dimensions.Y,
                                                             false,
                                                             SurfaceFormat.Color,
                                                             DepthFormat.Depth24Stencil8);

            base.Initialize();
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            canvas = new Canvas(GraphicsDevice);

            //TODO: use this.Content to load your game content here 

            var loadEvent = new LoadEvent(Content); // Invoke a LoadEvent.
            EventManager.CallEvent(loadEvent);
        }

        /// <summary>
        /// Stores the game time of the last second.
        /// </summary>
        internal double last = 0;
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            // For Mobile devices, this logic will close the Game when the Back button is pressed
            // Exit() is obsolete on iOS
#if !__IOS__ && !__TVOS__
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
#endif

            // TODO: Add your update logic here

            this.gameTime = gameTime;

            if (gameTime.TotalGameTime.TotalSeconds - last > 1) {               // Check if one second has passed since last second.
                last = gameTime.TotalGameTime.TotalSeconds;                     // Set last second to current time.
                var level = (Debugger.DebugLevel)(((int)last / 5) % 5);         // A debug level, changes every 5 seconds
                Debugger.Debug((int)last + " seconds passed", 1, false, level); // Debug current runtime (last second stores game uptime).
            }

            var updateEvent = new UpdateEvent(gameTime);    // Invoke an UpdateEvent.
            EventManager.CallEvent(updateEvent);

            base.Update(gameTime);
        }

        RenderTarget2D dest;

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.SetRenderTarget(dest);

            Color bufferDefault = BackgroundColor;                     // The default color for the frame.
            graphics.GraphicsDevice.Clear(bufferDefault);                   // Clears the buffer (frame) to the desired color.

            canvas.Begin();                                                 //
            RenderEvent renderEvent = new RenderEvent(gameTime, canvas);    //
            EventManager.CallEvent(renderEvent);
            canvas.End();                                                   //
                                                                            //
            GraphicsDevice.SetRenderTarget(null);

            canvas.Begin();
            canvas.Draw(dest, Vector2.Zero, Color.White);
            canvas.End();


            base.Draw(gameTime);                                            //
        }
    }
}

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame.API.Events;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Graphics.Text;

namespace ModopeGame {

    /// <summary>
    /// A class used for displaying debug messages in the game.
    /// </summary>
    [EventHandler]
    public static class Debugger {

        /// <summary>
        /// Debug level.
        /// Affects the color of the debug message.
        /// </summary>
        public enum DebugLevel {
            /// <summary>
            /// Level: 0
            /// Color: White
            /// Style: Regular
            /// </summary>
            Info     = 0,
            /// <summary>
            /// Level: 1
            /// Color: Dark Gray
            /// Style: Regular
            /// </summary>
            Debug    = 1,
            /// <summary>
            /// Level: 2
            /// Color: Green
            /// Style: Regular
            /// </summary>
            Alert    = 2,
            /// <summary>
            /// Level: 3
            /// Color: Yellow
            /// Style: Regular
            /// </summary>
            Warning  = 3,
            /// <summary>
            /// Level: 4
            /// Color: Red
            /// Style: Regular
            /// </summary>
            Critical = 4
        }

        /// <summary>
        /// Debug Message structure stores basic information used for displaying debug messages.
        /// </summary>
        internal class DebugMessage {
            /// <summary>
            /// The debug message.
            /// </summary>
            internal string data;
            /// <summary>
            /// Duration: Display duration in seconds.
            /// </summary>
            internal double duration;
            /// <summary>
            /// Should the message fade out when display duration ends.
            /// </summary>
            internal bool fade;
            /// <summary>
            /// Debug message color.
            /// </summary>
            internal Vector3 color;
        }

        /// <summary>
        /// This function returns a user friendly type name (Aka, for List<string> return 'List<string>')
        /// </summary>
        /// <returns>Friendly type name.</returns>
        internal static string GetFriendlyName(Type type) {
            string friendlyName = type.Name;                                        // Start off with the class name.
            if (type.IsGenericType) {                                               // If the type is generic (for example: List, Dictionary...):
                int iBacktick = friendlyName.IndexOf('`');                          // Get the index of the '`' (for some reason it is included in the type name).
                if (iBacktick > 0) {                                                // If the '`' is present:
                    friendlyName = friendlyName.Remove(iBacktick);                  // Remove everything after it (we remain with the class name).
                }                                                                   //
                friendlyName += "<";                                                // Add '<' to the name (because the name is generic)
                Type[] typeParameters = type.GetGenericArguments();                 // Get the list of generic types
                for (int i = 0; i < typeParameters.Length; i++) {                   // Iterate through the generic parameter types:
                    string typeParamName = GetFriendlyName(typeParameters[i]);      // Get the name of the generic parameter type (use recursion for extreme cases, for example: List<List<string>>).
                    friendlyName += (i == 0 ? typeParamName : ", " + typeParamName);// Add it to the friendly name (if multiple generic parameter type are present, seperate them with ', ').
                }                                                                   //
                friendlyName += ">";                                                // Close off friendly name with '>'.
            }                                                                       //
            return friendlyName;                                                    //
        }

        /// <summary>
        /// Converts an object to readable string (more advanced than '#.ToString()').
        /// </summary>
        internal static string ObjectToString(object obj) {
            string objStr;                                          // string reprensentation of the object.
            var enumerable = obj as System.Collections.IEnumerable; // Try to parse the object as an IEnumerable object (for example: List, Dictionary...).
            if (obj is string || enumerable == null)                // If the object is not an IEnumerable or a string:
                objStr = obj.ToString();                            // Use '#.ToString()' (string is char[], so we exlude it from the rest of list type objects).
            else {                                                  // Otherwise:
                objStr = GetFriendlyName(obj.GetType()) + "[\n";    // Start off with the object's type and an opening bracket.
                string lines = "";                                  // A string that will hold the string representing the elements.
                int count = 0;                                      // A counter (for checking if the IEnumerable does not contain items).
                foreach (var item in enumerable) {                  // Iterate over the elements in the object:
                    if (lines != "")                                // If lines isn't empty:
                        lines += "\n";                              // Add line break.
                    lines += ObjectToString(item);                  // Add each element to the string (use recursion for extreme cases, for example: List<List<string>>)
                    count++;                                        // Add 1 to the counter.
                }                                                   //
                if (count == 0)                                     // If no items were added:
                    objStr = (objStr + lines).Replace("\n", "");    // Remove line breaks.
                else                                                // Otherwise:
                    foreach (string line in lines.Split('\n'))      // Iterate over the elements string, splitted by line breaks.
                        objStr += "  " + line + "\n";               // Add the lines to the object string with an indention.
                objStr += "]";                                      // Close off with a closing bracket.
                if (obj.GetType().IsArray)                          // If object is an array:
                    objStr = objStr.Replace("[][", "[");            // Remove double brackets (array friendly names come out like this: object[], and we add another set of brackets anyways...).
            }                                                       //
            return objStr;                                          //
        }

        /// <summary>
        /// Breaks a long string into lines.
        /// </summary>
        /// <returns>The source string broken into lines.</returns>
        /// <param name="source">The source string.</param>
        internal static string BreakIntoLines(string source) {
            const int maxRowLength = 100;                           // The maximum length for a single line.
            string data = "";                                       // A string that will hold the end result.
            string[] originalLines = source.Split('\n');            // Don't mess up original line breaking, handle each line individually.
            foreach (string originalLine in originalLines) {        // Iterate over the original lines:
                string curr = originalLine;                         // Create a copy of the original line.
                string lines = "";                                  // A string that will hold the original line, broken into lines (if too long).
                while (curr.Length > 0) {                           // Loop until iterated over the entire line:
                    int high = Math.Min(maxRowLength, curr.Length); // The highest index for the line (in case line is shorter than maxRowLength).
                    string tmp = curr.Substring(0, high);           // A variable holding a part of the original line (a broken line).
                    if (lines != "")                                // If the lines string is empty:
                        lines += "\n";                              // Add line break.
                    lines += tmp;                                   // Add broken line to lines string.
                    curr = curr.Substring(high);                    // Remove broken line from the beginning of the temporary line copy variable.
                }                                                   //
                if (data != "")                                     // If the end result is empty:
                    data += "\n";                                   // Add line break.
                data += lines;                                      // Add the lines to the end result.
            }
            return data;
        }

        /// <summary>
        /// Draws the active debug messages to the screen.
        /// </summary>
        [CoreEventHandler(Priority = 10000)]
        public static void drawDebugMessages(RenderEvent e) {
            e.Canvas.Setup2D();
            GameTime gameTime = e.GameTime;                                                                             // Get game time from event.
            float y = 10;                                                                                               // Stores the debug message y position.
            float x = 10;                                                                                               // Stores the debug message x position.
            List<DebugMessage> remove = new List<DebugMessage>();                                                       // Create a list, storing the debug messages to remove (their duration has ended).
            foreach (DebugMessage msg in debugMessages) {                                                               // Iterate through the active debug messages:
                Vector4 color = new Vector4(msg.color, 1f);                                                             // Base color for the debug message's text.
                Vector4 backColor = new Vector4(0f, 0f, 0f, 0.5f);                                                      // Base color for the debug message's background.
                float multiplier = msg.duration > 0.5d ? 1f : (float)msg.duration * 2f;                                 // The multiplier - when the remainig duration aproaches 0, the multiplier get smaller.
                if (msg.fade) {                                                                                         // If the message is supposed to fade:
                    color *= multiplier;                                                                                // Apply multiplier to text color.
                    backColor *= multiplier;                                                                            // Apply multiplier to back color.
                }                                                                                                       // (We only apply the multiplier if needed).
                Vector2 dims = Fonts.DebugFont.MeasureString(msg.data);                                                 // Get the text dimmensions (width and height).
                Rectangle background = new Rectangle((int)x - 3, (int)y - 2, (int)dims.X + 6, (int)dims.Y + 4);         // Create the background rectangle (a bit bigger than the text bounds).
                e.Canvas.Draw(CommonResources.Pixel, background, new Color(backColor));                                 // Draw the background with the pixel texture and the background color.
                e.Canvas.DrawString(Fonts.DebugFont, msg.data, new Vector2(x, y), new Color(color));                    // Draw the text with the text color.
                y += dims.Y + 7;                                                                                        // Increase the text y position by the text height + 4 (so messages don't overlap).
                msg.duration -= gameTime.ElapsedGameTime.TotalMilliseconds / 1000d;                                     // Remove the passed time from the message's remaining duration.
                if (msg.duration < 0)                                                                                   // If the message's remaining duration is smaller of equals to 0:
                    remove.Add(msg);                                                                                    // Add the message to the remove list.
            }                                                                                                           //
            debugMessages.RemoveAll(m => remove.Contains(m));                                                           // Remove all the messages in the remove list from thelist.
        }

        /// <summary>
        /// Stores the currently active debug messages.
        /// </summary>
        internal static List<DebugMessage> debugMessages = new List<DebugMessage>();

        /// <summary>
        /// Debugs a message (or an object).
        /// Level: Info
        /// </summary>
        /// <param name="message">The message to debug.</param>
        /// <param name="duration">Duration in seconds.</param>
        /// <param name="fade">If set to <c>true</c> fade out will be used.</param>
        public static void Info(object message, double duration = 3d, bool fade = true) {
            Custom(message, duration, fade, Color.White.ToVector3());
        }

        /// <summary>
        /// Debugs a message (or an object).
        /// Level: Debug
        /// </summary>
        /// <param name="message">The message to debug.</param>
        /// <param name="duration">Duration in seconds.</param>
        /// <param name="fade">If set to <c>true</c> fade out will be used.</param>
        /// <param name="level">Debug level. Debug by default.</param>
        public static void Debug(object message, double duration = 3d, bool fade = true, DebugLevel level = DebugLevel.Debug) {
            switch(level) {
                case DebugLevel.Info:
                    Info(message, duration, fade);
                    break;
                case DebugLevel.Alert:

					Alert(message, duration, fade);
                    break;
                case DebugLevel.Warning:

					Warning(message, duration, fade);
                    break;
                case DebugLevel.Critical:

					Critical(message, duration, fade);
                    break;
                default:
                    Custom(message, duration, fade, Color.DarkGray.ToVector3());
                    break;
            }
        }

        /// <summary>
        /// Debugs a message (or an object).
        /// Level: Alert
        /// </summary>
        /// <param name="message">The message to debug.</param>
        /// <param name="duration">Duration in seconds.</param>
        /// <param name="fade">If set to <c>true</c> fade out will be used.</param>
        public static void Alert(object message, double duration = 3d, bool fade = true) {
            Custom(message, duration, fade, Color.Lime.ToVector3());
        }

        /// <summary>
        /// Debugs a message (or an object).
        /// Level: Warning
        /// </summary>
        /// <param name="message">The message to debug.</param>
        /// <param name="duration">Duration in seconds.</param>
        /// <param name="fade">If set to <c>true</c> fade out will be used.</param>
        public static void Warning(object message, double duration = 3d, bool fade = true) {
            Custom(message, duration, fade, Color.Yellow.ToVector3());
        }

        /// <summary>
        /// Debugs a message (or an object).
        /// Level: Critical
        /// </summary>
        /// <param name="message">The message to debug.</param>
        /// <param name="duration">Duration in seconds.</param>
        /// <param name="fade">If set to <c>true</c> fade out will be used.</param>
        public static void Critical(object message, double duration = 3d, bool fade = true) {
            Custom(message, duration, fade, Color.Red.ToVector3());
        }

        /// <summary>
        /// Debugs a message (or an object).
        /// Level: Debug
        /// </summary>
        /// <param name="message">The message to debug.</param>
        /// <param name="duration">Duration in seconds.</param>
        /// <param name="fade">If set to <c>true</c> fade out will be used.</param>
        public static void Custom(object message, double duration = 3d, bool fade = true, Vector3 color = default(Vector3)) {
            string messageStr = ObjectToString(message);    // Get the string representation of the debugged object.
            DebugMessage obj = new DebugMessage() {         // Create a new DebugMessage instance.
                data = BreakIntoLines(messageStr),          // In case debug message is too long, it will be broken into lines.
                duration = duration,                        // 
                fade = fade,                                // Fade is true by default, just for the nice look.
                color = color,                              // Depends on message level.
            };                                              // 
            debugMessages.Add(obj);                         // Add the message to the currently active debug messages.
        }
    }
}

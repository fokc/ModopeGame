﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Xna.Framework.Content;

namespace ModopeGame.Content {
    /// <summary>
    /// An extension to the ContentManager Class
    /// </summary>
    public class AdvancedContentManager : ContentManager {

        private const char SEPERATOR = '§';

        /// <summary>
        /// Constructor, just copies data from existing ContentManager.
        /// </summary>
        protected internal AdvancedContentManager(IServiceProvider serviceProvider)
            : base(serviceProvider) { }

        /// <summary>
        /// Loads content in the original content loading method (content pipeline).
        /// </summary>
        public override T Load<T>(string assetName) {
            if (assetName == null) return default(T);
            try {
                return LoadEmbedded<T>(assetName);                
            } catch {
                try {
                    return LoadFile<T>(assetName);
                } catch {
                    return LoadContent<T>(assetName);
                }
            }
        }

        Assembly loadingAssembly = null;
        /// <summary>
        /// Loads an embedded XNB file.
        /// </summary>
        /// <param name="assetName">Embedded XNB file name (namespace + filename).
        /// .xnb Ending is optional.</param>
        public T LoadEmbedded<T>(string assetName) {
            assetName = assetName.Replace("/", ".").Replace("\\", ".");
            if (!assetName.EndsWith(".xnb"))                    // If .xnb ending is not present:
                assetName += ".xnb";                            // Add it.
            loadingAssembly = Assembly.GetCallingAssembly();    // Set loading assembly to the calling assembly.
            return base.Load<T>("Embedded" + SEPERATOR + assetName);
        }

        /// <summary>
        /// Loads an XNB file.
        /// </summary>
        /// <param name="assetName">.xnb file path.
        /// .xnb Ending is optional.</param>
        public T LoadFile<T>(string assetName) {
        if (!assetName.EndsWith(".xnb"))    // If .xnb ending is not present:
            assetName += ".xnb";            // Add it.
            return base.Load<T>("File" + SEPERATOR + assetName);
        }

        /// <summary>
        /// Default content loading operation through the content pipeline.
        /// </summary>
        public T LoadContent<T>(string assetName) {
            return base.Load<T>(assetName);
        }

        /// <summary>
        /// Opens the embedded file stream from file path.
        /// </summary>
        public Stream OpenEmbeddedFileStream(string embeddedFilePath) {
            embeddedFilePath = embeddedFilePath.Replace("/", ".").Replace("\\", ".");
            loadingAssembly = Assembly.GetCallingAssembly();    // Set loading assembly to the calling assembly.
            return OpenEmbeddedStream(embeddedFilePath);
        }

        /// <summary>
        /// Opens stream for an embedded file.
        /// </summary>
        public Stream OpenEmbeddedStream(string embeddedFileName) {
            var stream = loadingAssembly.GetManifestResourceStream(embeddedFileName);   // Load embedded file from loading assembly.
            loadingAssembly = null;                                                     // Reset loading assembly.
            if(stream == null)                                                          // If the file could not be found:
                foreach(Assembly asm in AppDomain.CurrentDomain.GetAssemblies()) {      // For each loaded assembly:
                    foreach (string name in asm.GetManifestResourceNames())             // Iterate over embedded files.
                        if (name.EndsWith(embeddedFileName)) {                          // If the files match:
                            stream = asm.GetManifestResourceStream(name);               // Load the file.
                            if (stream != null)                                         // If loaded successfuly:
                                break;                                                  // Break loop.
                        }                                                               //
                    if (stream != null)                                                 // If loaded successfuly:
                        break;                                                          // Break loop.
                }
            return stream; 
        }

        /// <summary>
        /// Opens stream for a file.
        /// </summary>
        public Stream OpenFileStream(string filePath) {
            return File.OpenRead(filePath);
        }

        protected override Stream OpenStream(string assetName) {
            string[] args = assetName.Split(new char[] { SEPERATOR }, 2);   // Split the assetName so we can get the asset type.
            Func<string, Stream> openStreamFunc = base.OpenStream;          // OpenStream function to use to open the file stream.
            if (args.Length > 0)                                            // If asset type is specified:
                if (args[0] == "Embedded")                                  // If specified asset type is Embedded:
                    openStreamFunc = OpenEmbeddedStream;                    // Set OpenStream function to OpenEmbeddedStream.
                else if (args[0] == "File")                                 // Otherwise if specified asset type is File:
                    openStreamFunc = OpenFileStream;                        // Set OpenStream function to OpenFileStream.
            return openStreamFunc(args[args.Length - 1]);                   // Call the OpenStream function.
        }
    }
}

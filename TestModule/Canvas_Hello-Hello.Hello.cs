﻿using System;
using Microsoft.Xna.Framework;
using ModopeGame.API.Graphics;

namespace TestModule {
    public static class Canvas_Hello {
        public static void renderHello(this Canvas canvas) {
            canvas.DrawString(Main.arial, "Hello!", new Vector2(200, 200), Color.Black);
        }
    }
}

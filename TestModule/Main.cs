﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ModopeGame;
using ModopeGame.API.Events;
using ModopeGame.API.Events.Core;
using ModopeGame.API.Events.Input;
using ModopeGame.API.Events.Input.Events;
using ModopeGame.API.Graphics;
using ModopeGame.API.Graphics.Shapes;
using ModopeGame.API.Graphics.Text;
using ModopeGame.API.Modules;

namespace TestModule {

    [EventHandler]
    [Module("test", "Test Module", "0.1")]
    public class Main : Module {

        [EventHandler]
        public void Init(InitializationEvent e) {
            Core.IsFixedTimeStep = false;
        }

        public static SpriteFont arial;
        public static Texture2D texture;
        public static Model model;
        [EventHandler]
        public void Load(LoadEvent e) {
            texture = e.Content.LoadEmbedded<Texture2D>("TestModule.just_monika");
            arial = e.Content.LoadEmbedded<SpriteFont>("TestModule.Arial.xnb");
            model = e.Content.LoadEmbedded<Model>("gun");
        }


        const float speed = 5;

        const int inverseY = 1;
        const int inverseX = -1;

        [EventHandler]
        public void OnMouseMove(MouseMoveEvent e) {
            pitch += e.Delta.Y * (float)e.GameTime.ElapsedGameTime.TotalSeconds * speed * inverseY;
            yaw += e.Delta.X * (float)e.GameTime.ElapsedGameTime.TotalSeconds * speed * inverseX;
            e.Canceled = true;

            yaw %= 360;
            pitch %= 360;
            roll %= 360;
        }


        [EventHandler]
        public void OnKeyHeld(KeyHeldEvent e) {
            velocity = new Vector3(0, 0, 0);

            if (e.KeysHeld.Contains(Keys.W)) {
                Debugger.Debug("test1", 0.001, false);
                velocity += dir;
            }
            if (e.KeysHeld.Contains(Keys.S)) {
                Debugger.Debug("test2", 0.001, false);
                velocity -= dir;
            }

            if (e.KeysHeld.Contains(Keys.A))
                velocity += GetStrafe(-90);
            if (e.KeysHeld.Contains(Keys.D))
                velocity += GetStrafe(90);

            pos += velocity * (float)e.GameTime.ElapsedGameTime.TotalSeconds * speed;
        }

        [EventHandler]
        public void Update(UpdateEvent e) {

            //if (keyboard.IsKeyDown(Keys.Up))
            //    pitch += 2;
            //if (keyboard.IsKeyDown(Keys.Down))
            //    pitch -= 2;
            //if (keyboard.IsKeyDown(Keys.Right))
            //    yaw += 2;
            //if (keyboard.IsKeyDown(Keys.Left))
            //    yaw -= 2;

        }

        [EventHandler]
        public void Render(RenderEvent e) {
            e.Canvas.Setup3D();

            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in model.Meshes) {
                foreach (BasicEffect effect in mesh.Effects) {
                    effect.View = view;
                    effect.Projection = projection;
                    effect.World = transforms[mesh.ParentBone.Index] *
                        modelWorld;
                }
                mesh.Draw();
            }

        }






        Vector3 pos = new Vector3(0, 0, 0);
        float yaw = 0, pitch = 0, roll = 0;
        Vector3 dir {
            get {
                float rad_yaw = MathHelper.ToRadians(yaw);
                float rad_pitch = MathHelper.ToRadians(pitch);
                float rad_roll = MathHelper.ToRadians(roll);
                return Vector3.Transform(Vector3.UnitZ, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
            }
        }
        Vector3 up {
            get {
                float rad_yaw = MathHelper.ToRadians(yaw);
                float rad_pitch = MathHelper.ToRadians(pitch);
                float rad_roll = MathHelper.ToRadians(roll);
                return Vector3.Transform(Vector3.UnitY, Matrix.CreateFromYawPitchRoll(rad_yaw, rad_pitch, rad_roll));
            }
        }
        Vector3 target { get { return pos + dir; } }

        Vector3 modelPos = new Vector3(0, 0, 10);
        Vector3 modelYawPitchRoll = new Vector3(0, 0, 0);
        Vector3 modelScale = new Vector3(1, 1, 1) * 0.02f;
        Matrix modelWorld {
            get {
                return Matrix.CreateScale(modelScale)
                             * Matrix.CreateFromYawPitchRoll(modelYawPitchRoll.X, modelYawPitchRoll.Y, modelYawPitchRoll.Z)
                             * Matrix.CreateTranslation(modelPos);
            }
        }

        Matrix view { get { return Matrix.CreateLookAt(pos, target, up); } }
        Matrix projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45), 1920f / 1080f, 0.1f, 500f);

        private Vector3 GetStrafe(float yawOffset) {
            float rad_yaw = -MathHelper.ToRadians(yawOffset);
            Vector3 transformed = Vector3.Transform(dir, Matrix.CreateFromYawPitchRoll(rad_yaw, 0, 0));
            return new Vector3(transformed.X, 0, transformed.Z);
        }

        Vector3 velocity = new Vector3(0, 0, 0);
    }
}
